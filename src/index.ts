// TheMas3212

const express = require('express');
const jsonfile = require('jsonfile');
const app = express();
const port = 3000;
const number_of_signifigant_digits = 5

type data = { wins: number, losses: number }
let data: data = { wins:0, losses:0 }
let ratio: string = "0";

const settingsFile = process.env.HOME + '/.jeff.json'
jsonfile.readFile(settingsFile)
    .then(obj => {
        data = obj;
    })
    .catch(error => {
        data = { wins: 0, losses: 0 };
    })
    .finally(()=>{
        ratio = (data.wins/data.losses).toFixed(number_of_signifigant_digits);
    })


function build_page(req: Express.Request, res: Express.Response) {
    let resp = `<center><h1>Wins: ${data.wins}</h1></center>\n` +
               `<center><h1>Losses: ${data.losses}</h1></center>\n` +
               `<center><h2>Win/Loss Ratio: ${ratio}</h2></center>\n` +
               `<center><form action="win" method="post"><button type="submit">Add Win</button></form>` +
               `<form action="loss" method="post"><button type="submit">Add Loss</button></form></center>`
    res.send(resp);
}

function add_win() {
    data.wins += 1
    ratio = (data.wins/data.losses).toFixed(number_of_signifigant_digits)
}

function add_loss() {
    data.losses += 1
    ratio = (data.wins/data.losses).toFixed(number_of_signifigant_digits)
}

app.get('/', build_page);
app.post('/win', (req: Express.Request, res: Express.Response) => {
    add_win();
    jsonfile.writeFile(settingsFile, data, { flag: 'w+' }, (err) => {
        console.error(err);
    });
    res.redirect('/');
});
app.post('/loss', (req: Express.Request, res: Express.Response) => {
    add_loss();
    jsonfile.writeFile(settingsFile, data, { flag: 'w+' }, (err) => {
        console.error(err);
    });
    res.redirect('/');
});

app.listen(port, () => console.log(`Listening on port ${port}!`));